package com.taskaprosthetics.trainingtest

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.Button
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.random.Random

/**
 * Custom game view for displaying the actual game within another activity
 */
class MyGameView@JvmOverloads constructor(context:Context,attrs:AttributeSet?=null,defStyle:Int=0,defStyleRes:Int=0):View(context,attrs,defStyle,defStyleRes){

    var moving = false


    private val squareSize = 50
    private var squareLeft = -width
    private var squareTop = 0
    private var squareColor = Color.BLACK

    // Code for moving the square conditionally
    init {
        GlobalScope.launch {
            while(true){
                if(moving){
                    squareLeft += 10
                    if(squareLeft > width) {
                        squareLeft = -width
                        squareTop += squareSize
                    }
                    if (squareTop > height) {
                        squareTop = 0
                    }
                }

                Thread.sleep(10)
            }
        }
    }

    // Render the square on the screen, position changes using co-routine started on initialisation
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        val squarePaint = Paint().apply {
            color = squareColor
        }

        val square = Rect(
            squareLeft, squareTop, squareLeft+squareSize, squareTop+squareSize
        )

        canvas!!.drawRect(square, squarePaint)

        invalidate()
        requestLayout()
        forceLayout()
    }

    fun setRandomColor() {

        squareColor = Color.rgb(Random.nextInt(0, 255), Random.nextInt(0, 255), Random.nextInt(0, 255))

    }
}